#!/bin/bash

PRG=libdmyeti

# ---------------------------------------------------------
YETI=/dm/dmYeti/tools/yeti.jar
case $1 in
project*)
  mkdir src
  mkdir bin
  mkdir lib
  mkdir pack
  mkdir tests
  mkdir tests/src
  mkdir tests/classes
  echo 'fun main(args: Array<String>) {' > src/Main.kt
  echo '    println("Hello, World!")' >> src/Main.kt
  echo '}' >> src/Main.kt
  ;;
c*)
  java -jar $YETI -d classes src/*.yeti
  java -jar $YETI -cp classes -d tests/classes tests/src/all.yeti
  if [ -e tests/resources ]
  then
    rm -fR test/resources
    cp -fR tests/resources tests/classes
  fi
  ;;
x*)
  java -jar $YETI -d classes src/*.yeti
  java -jar $YETI -cp classes -d tests/classes tests/src/all.yeti
  if [ -e tests/resources ]
  then
    rm -fR test/resources
    cp -fR tests/resources tests/classes
  fi
  java -cp $YETI:classes:tests/classes all
  ;;
pack*)
  java -jar $YETI -d classes src/*.yeti
  if [ -e pack/tmp ]
  then
    rm -fR pack/tmp
  fi
  mkdir pack/tmp
  cd pack/tmp
#  jar xvf $YETI
  cp -fR ../../classes/* ./
#  rm -fR META-INF
  mkdir META-INF
  echo 'Manifest-Version: 1.0' > META-INF/MANIFEST.MF
  echo 'Created-By: ºDeme' >> META-INF/MANIFEST.MF
#  echo 'Main-Class: Main' >> META-INF/MANIFEST.MF
  echo ''>> META-INF/MANIFEST.MF
  jar cvfm ../$PRG.jar META-INF/MANIFEST.MF *
  cd ../..
  rm -fR pack/tmp
  ;;
*)
  echo $1: Unknown option
  ;;
esac
