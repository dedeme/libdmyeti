// Copyright 19-Aug-2018 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

module actorTest;

load es.dm.exc;
actor = load es.dm.actor;
load es.dm.std;
date = load es.dm.date;

(
typedef barberT = {
    timeout is number,
    dateStart is date.t,
    ctimeout is number,
    var cdateStart is date.t,
    var sleeping is boolean
  };

typedef clientT = {
    name is string
  };

typedef barbery =
  {
    var open is boolean,
    var clients is list<clientT>,
    var clientCutting is option<clientT>
  };

ac = actor.mk 50;

barbery = {var open = false, var clients = [], var clientCutting = None ()};

{
  sitsToStr () = fold (_ r c = r ^ "[\(c.name)]") "" barbery.clients,

  mkBarber timeout ctimeout =
    {
      timeout,
      dateStart = date.now (),
      ctimeout,
      var cdateStart = date.now (),
      var sleeping = true
    },

  startBarber b = (
    barbery.open := true;
    date.dfMillis (date.now ()) b.dateStart < b.timeout loop (
      case barbery.clientCutting of
      None (): ();
      Some c:
        if date.dfMillis (date.now ()) b.cdateStart > b.ctimeout
        then actor.send ac \(
          barbery.clientCutting := None ();
          println "\(c.name): Haircut finished";
          case barbery.clients of
          c::rest: (
            barbery.clientCutting := Some c;
            b.cdateStart := date.now ();
            barbery.clients := rest;
            println "\(c.name): Haircut started \(sitsToStr ())");
          _: b.sleeping := true
          esac)
        fi
      esac;
      sleep 100);
    barbery.open := false;

    not b.sleeping loop (
      case barbery.clientCutting of
      None (): throw (illegalState ("There is no client cutting hair"));
      Some c:
        if date.dfMillis (date.now ()) b.cdateStart > b.ctimeout
        then actor.send ac \(
          barbery.clientCutting := None ();
          println "\(c.name): Haircut finished";
          case barbery.clients of
          c::rest: (
            barbery.clientCutting := Some c;
            b.cdateStart := date.now ();
            barbery.clients := rest;
            println "\(c.name): Haircut started \(sitsToStr ())");
          _: b.sleeping := true
          esac)
        fi;
      esac;
      sleep 100);
    actor.stop ac),

  launchClients b = for [0..(int b.timeout * 1.2 / b.ctimeout)] do i:
      c = {name = (string i)};
      thread \(
          println "\(c.name): Created";
          if barbery.open then actor.send ac \(
            if b.sleeping
            then
              barbery.clientCutting := Some c;
              b.cdateStart := date.now ();
              b.sleeping := false;
              println "\(c.name): Haircut started \(sitsToStr ())"
            else
              if length barbery.clients < 5
              then
                barbery.clients :=
                  barbery.clients |> reverse |> (c::) |> reverse;
                println "\(c.name): Takes a sit \(sitsToStr ())"
              else
                println "\(c.name): Goes away"
              fi
            fi)
          else println "\(c.name): Barbery is closed"
          fi;
        );
      b.ctimeout * 0.9 |> int |> randomInt |> sleep
    done,

  run () =
    println "Test actor";

    actor.start ac;
    b = mkBarber 5000 100;

    thread \(launchClients b);
    thread \(startBarber b);


    println "    Finished"
})