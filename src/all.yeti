// Copyright 06-Aug-2018 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Load all es.dm library.
///
/// Notes:
/// : Module es.dm.std is directly available through this module.
/// : Modules json and rbox should not be loaded as name space, because they
/// :   have free variables.
/// : [tutorial](http://localhost/data/Short_introduction_to_Yeti.html)
/// : [yeti.lang.std](http://localhost/data/yeti.lang.std.html)

module es.dm.all;

load es.dm.std;

typedef option<a> = option;

actor = load es.dm.actor;
/// `[actorT]` is the type of actors.
typedef actorT = actor.t;

b64 = load es.dm.b64;

buf = load es.dm.buf;

/// `[bufT]` is the type of a text buffer.
typedef bufT = buf.t;

/// `[bufT]` is the type of a binary buffer.
typedef bufBinT = buf.bT;

cgi = load es.dm.cgi;

cryp = load es.dm.cryp;

dec = load es.dm.dec;

date = load es.dm.date;

/// [dateT] is the type of dates.
typedef dateT = date.t;

exc = load es.dm.exc;

file = load es.dm.file;

io = load es.dm.io;

path = load es.dm.path;

sys = load es.dm.sys;

zip = load es.dm.zip;

{
  /// `[p1 ^/ p2]` concatenates paths 'p1' and 'p2'.
  (^/) = path.(^/),

  /// `[none? o]` returns true if option 'o' is None ().
  none? = none?,

  /// `[value o]` return the Some value of 'o' or throws an exception if
  /// 'o' is None ()
  optGet = optGet,

  /// `[cindex ch s]` returns the index of the first occurrence of the firts
  /// character of 'ch' in 's', or 'None ()' if such occurrence does not exist.
  cindex = cindex,

  /// `[cindex' ch start s]` returns the index of the first occurrence of the
  /// firts character of 'ch' from 'start' in 's', or 'None ()' if such
  /// occurrence does not exist.
  cindex' = cindex',

  /// `[sindex sub s]` returns the index of the first occurrence of 'sub'
  /// in 's', or 'None ()' if such occurrence does not exist.
  sindex = sindex,

  /// `[sindex' sub start s]` returns the index of the first occurrence of 'sub'
  /// from 'start' in 's', or 'None ()' if such occurrence does not exist.
  sindex' = sindex',

  /// `[csplit ch s]` makes a list splitting 's' with the first character of
  /// 'ch'. This function is equivalent to array.split of javascript. For
  /// example:
  /// : assert(["a", "b"] == csplit ";" "a;b");
  /// : assert(["a", "b", ""] == csplit ";" "a;b;");
  /// : assert(["", "a"] == csplit ";" ";a");
  /// : assert(["",""] == csplit ";" ";");
  /// : assert(["a"] == csplit ";" "a");
  /// : assert([""] == csplit ";" "");
  csplit = csplit,

  /// `[csplit sep s]` makes a list splitting 's' with the 'sep.
  /// This function is equivalent to array.split of javascript.
  /// : If sep is "" then the function returns a list with each character of
  /// : 's'. In this case if s is "" too, it returns '[]'.
  ssplit = ssplit,

  /// `[thread f]` Creates and runs a thread which executes 'f'.
  thread = thread,

  /// `[sleep millis`] stops the current thread 'millis' milliseconds.
  sleep = sleep,

  /// `[b64]` namespace.
  b64 = b64,
  /// `[path]` namespace.
  path = path,
  /// `[buf]` namespace.
  buf = buf,
  /// `[cgi]` namespace.
  cgi = cgi,
  /// `[cryp]` namespace.
  cryp = cryp,
  /// `[exc]` namespace.
  exc = exc,
  /// `[file]` namespace.
  file = file,
  /// `[io]` namespace.
  io = io,
  /// `[sys]` namespace.
  sys = sys,
  /// `[dec]` namespace.
  dec = dec,
  /// `[date]` namespace.
  date = date,
  /// `[zip]` namespace.
  zip = zip,
}
