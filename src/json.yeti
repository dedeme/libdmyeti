// Copyright 03-Aug-2018 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Json encoder and decoder.
module es.dm.json;

load es.dm.std;
buf = load es.dm.buf;
dec = load es.dm.dec;

typedef json =
    Null ()
  | Boolean boolean
  | Number number
  | String string
  | Array array<json>
  | Object hash<string, json>;

class JsonException(String msg) extends Exception(msg) end;

(
  raise msg = throw new JsonException(msg);

  mkTx s = {s, bg = 0, end = strLength s};

  len tx = tx.end - tx.bg;

  move s n = s with {bg = s.bg + n};

  starts tx s =
    (st tx s' =
        if len s' == 0 then true
        elif strChar tx.s tx.bg != strChar s'.s s'.bg then false
        else st (move tx 1) (move s' 1)
        fi;
     st tx (mkTx s));

  skipBlanks tx =
    if len tx == 0 then tx
    elif strChar tx.s tx.bg <= " " then skipBlanks (move tx 1)
    else tx
    fi;

  jnull tx =
    if starts tx "null" then {js = Null (), tx = move tx 4}
    else raise "Expected value 'null'"
    fi;

  jtrue tx =
    if starts tx "true" then {js = Boolean true, tx = move tx 4}
    else raise "Expected value 'true'"
    fi;

  jfalse tx =
    if starts tx "false" then {js = Boolean false, tx = move tx 5}
    else raise "Expected value 'false'"
    fi;

  jstring tx =
    (bf = buf.mk ();
     jst tx =
      if len tx == 0 then raise "Unexpected end of text reading a string"
      else
        case strChar tx.s tx.bg of
        "\"": {js = String (buf.toStr bf), tx = move tx 1};
        "\\":
          if len tx < 2 then raise "Unexpected end of text reading a string"
          else
            case strChar tx.s (tx.bg + 1) of
              "b": buf.add bf "\b"; jst (move tx 2);
              "f": buf.add bf "\f"; jst (move tx 2);
              "n": buf.add bf "\n"; jst (move tx 2);
              "r": buf.add bf "\r"; jst (move tx 2);
              "t": buf.add bf "\t"; jst (move tx 2);
              ch: buf.add bf ch; jst (move tx 2)
            esac
          fi;
        ch: buf.add bf ch; jst (move tx 1);
        esac
      fi;
     jst tx);

  jnumber tx =
    (bf = buf.mk ();
     jnm tx =
        if len tx == 0 then tx
        else
          c = strChar tx.s tx.bg;
          if (c >= "0" and c <= "9") or
              c == "." or c == "+" or c == "-" or c == "e" or c == "E"
          then buf.add bf c; jnm (move tx 1)
          else tx
          fi
        fi;
     tx = jnm tx;
     case dec.ofStr (buf.toStr bf) of
     None (): raise "Bad number";
     Some n: {js = Number n, tx}
     esac);

  of_s tx = (
    jarray tx = (
      jarr ls tx =
        ( {js, tx} = of_s tx;
          ls = js::ls;
          tx = skipBlanks tx;
          case strChar tx.s tx.bg of
          "]": {js = Array (array (reverse ls)), tx = move tx 1};
          ",": jarr ls (skipBlanks (move tx 1));
          ch: raise "Found '\(ch)' when expected ',' or ']' in array"
          esac);
      if len tx == 0 then raise "Unexpected end of text reading an array" fi;
      if strChar tx.s tx.bg == "]"
      then {js = Array (array []), tx = move tx 1}
      else jarr [] tx
      fi);

    jobject tx = (
      if len tx == 0 then raise "Unexpected end of text reading an object" fi;
      if strChar tx.s tx.bg == "}"
      then {js = Object [:], tx = move tx 1}
      else
        h = [:];
        jobj tx = (
          if strChar tx.s tx.bg == "\""
          then
            {js = jsKey, tx} = jstring (move tx 1);
            key = case jsKey of String k: k; _: "ERROR" esac;
            tx = skipBlanks tx;
            if (strChar tx.s tx.bg == ":")
            then
              {js, tx} = of_s (skipBlanks (move tx 1));
              h[key] := js;
              tx = skipBlanks tx;
              case strChar tx.s tx.bg of
              "}": move tx 1;
              ",": jobj (skipBlanks (move tx 1));
              ch: raise "Found '\(ch)' when expected ',' or '}' in object"
              esac
            else raise "Expected ':' reading an object"
            fi

          else raise "Object key is not a string"
          fi);
        tx = jobj tx;
        {js = Object h, tx}
      fi);

    case strChar tx.s tx.bg of
    "n": jnull tx;
    "t": jtrue tx;
    "f": jfalse tx;
    "\"": jstring (move tx 1);
    "-": jnumber tx;
    "[": jarray (skipBlanks (move tx 1));
    "{": jobject (skipBlanks (move tx 1));
    ch:
      if ch >= "0" and ch <= "9" then jnumber tx
      else raise "Found '\(ch)' when expected a value"
      fi
    esac);

  strToJs s = (
    tx = mkTx s;
    bf = buf.mk ();
    add s = buf.add bf s;
    stoj tx = (
      inc () = move tx 1;
      if len tx < 1 then buf.toStr bf
      else
        case strChar tx.s tx.bg of
        "\"": add "\\\""; stoj (inc ());
        "\\": add "\\\\"; stoj (inc ());
        "\b": add "\\b"; stoj (inc ());
        "\f": add "\\f"; stoj (inc ());
        "\n": add "\\n"; stoj (inc ());
        "\r": add "\\r"; stoj (inc ());
        "\t": add "\\t"; stoj (inc ());
        ch: add ch; stoj (inc ());
        esac
      fi);
    stoj tx
  );

{
  /// [ofStr s] returns a json object from a string JSON.
  jsOfStr s =
    {js, tx} = of_s (skipBlanks (mkTx s));
    if len (skipBlanks tx) > 0 then  raise "Expected en of text"
    else js
    fi,

  /// `[to_str js]` returns a string JSON which represents a 'json' object.
  jsToStr js is json -> string =
    case js of
    Null (): "null";
    Boolean v: if v then "true" else "false" fi;
    Number v: string v;
    String v: "\"" ^ (strToJs v) ^ "\"";
    Array v: "[" ^ strJoin "," (map jsToStr v) ^ "]";
    Object h:
      var ls = [];
      forHash h
        (_ k v = ls := ("\"" ^ (strToJs k) ^ "\"" ^ ":" ^ (jsToStr v))::ls);
      "{" ^ (strJoin "," ls)  ^ "}"
    esac,

  /// `[rarray js]` reads an Array JSON or raises an JsonException.
  rarray js is json -> array<json> =
    case js of Array a: a; _: raise "Array"; array [] esac,

  /// `[rbool js]` reads a Bool JSON or raises an JsonException.
  rboolean js is json -> boolean =
    case js of Boolean v: v; _: raise "Boolean"; false esac,

  /// [rhash f js] reads an Object JSON or raises an JsonException:
  ///
  /// Notes:
  /// : return is a 'hash' whose values are changed applying 'f' to them.
  rhash f js is (json -> 'a) -> json -> hash<string, 'a> =
    case js of
    Object o: r = [:]; forHash o (_ k v = r[k] := f v); r;
    _: raise "Object-rhash"; [:]
    esac,

  /// `[rnumber js]` reads an Number JSON or raises an JsonException.
  rnumber js is json -> number =
    case js of Number n: n; _: raise "Number"; 0 esac,

  /// `[rlist f js]` reads an Array JSON or raises an JsonException.
	/// : Return elements of Array JSON applying 'f' to them.
  rlist f js = map f (rarray js),

  /// `[robject js]` reads an Object JSON or raises an JsonException.
  robject js = case js of Object o: o; _: raise "Object"; [:] esac,

  /// `[ropt f js]` reads a Null JSON or 'Some (f js)'.
  ropt f js = case js of Null (): None (); v: Some (f v) esac,

  /// `[wopt f o]` writes a Null JSON if 'o' is None o 'f v' if it is 'Some v'.
  rstring js = case js of String s: s; _: raise "String"; "" esac,

  /// `[warray v]` returns the JSON value of 'v'.
  warray a = Array a,

  /// `[wbool v]` returns the JSON value of 'v'.
  wboolean v = Boolean v,

  /// `[whash f it]` writes an Object JSON using the converter 'f'.
  whash f h =
    r = [:];
    forHash h (_ k v = r[k] := f v);
    Object r,

  /// `[wfloat v]` returns the JSON value of 'v'.
  wnumber n = Number n,

  /// [wlist f it] writes an Array JSON using the converter 'f'.
  wlist f l = Array (array (map f l)),

  /// `[wobject v]` returns the JSON value of 'v'.
  wobject o = Object o,

  wopt f o =
    case o of None (): Null (); Some e: f e; esac,

  /// `[wstring v]` returns the JSON value of 'v'.
  wstring s = String s

}
)
