// Copyright 07-Aug-2018 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Zip utility.

module es.dm.zip;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipInputStream;

file = load es.dm.file;
path = load es.dm.path;


(
  (^/) = path.(^/);

  zipFile source zipName target = (
    fos = new FileOutputStream(target as ~String);
    zipOut = new ZipOutputStream(fos);
    fis = new FileInputStream(source as ~String);

    zipEntry = new ZipEntry(zipName as ~String);
    zipOut#putNextEntry(zipEntry);
    bytes = array[1..8192] as ~byte[];
    read len =
      if len >= 0 then
        zipOut#write(bytes, 0, len);
        read fis#read(bytes);
      fi;
    read fis#read(bytes);
    zipOut#close();
    fis#close();
    fos#close());

  zipDir source zipName target =
    for (file.dir source) do e:
      if e.directory? then
        zipDir (source ^/ e.name) (zipName ^/ e.name) target
      else
        zipFile source zipName target
      fi
    done;

{
  /// `[zip source target]` zips a file o directory ('source') in a zip file
  /// (target).
  ///
  /// Examples:
  /// : zip app/data tmp/data.zip
  zip source target =
    if file.directory? source then zipDir source (path.name source) target
    else zipFile source (path.name source) target
    fi,

  /// `[unzip source target]` unzips a zip file ('source') in a directory
  /// ('target').
  ///
  /// Examples:
  /// : unzip tmp/data.zip app
  unzip source target =
    bytes = array[1..8192] as ~byte[];
    zis = new ZipInputStream(new FileInputStream(source as ~String));
    write zipEntry =
      if defined? zipEntry then
        newFile = target ^/ (zipEntry as ~ZipEntry)#getName();
        fos = new FileOutputStream(newFile as ~String);
        wr len =
          if len > 0 then
            fos#write(bytes, 0, len);
            wr zis#read(bytes)
          fi;
        wr zis#read(bytes);
        fos#close();
        write zis#getNextEntry()
      fi;
    write zis#getNextEntry();
    zis#closeEntry();
    zis#close()
}

)