// Copyright 03-Aug-2018 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Synchronizer
module es.dm.actor;

import java.util.Vector;
import java.lang.Thread;

load es.dm.std;

typedef t = {
    var cont is boolean,
    queue is ~Vector,
    millis is number
  };

{
  /// `[mk millis`] creates an actor which read messages each 'millis'
  /// milliseconds.
  mk millis is number -> t =
    {
      queue = new Vector(),
      var cont = true,
      millis
    },

  /// `[start ac]` runs the actor 'ac' its own thread.
  start ac is t -> () = thread \(
    ac.cont loop
      if (ac.queue is ~Vector)#isEmpty() then Thread#sleep(ac.millis)
      else (ac.queue#remove(0) unsafely_as () -> ())()
      fi),

  /// `[send ac f]` sends to actor 'ac' the function 'f' to be executed in a
  /// synchronized way.
  send ac f = if (ac.queue is ~Vector)#add(f) then () fi,

  /// `[wait ac f]` sends to actor 'ac' the function 'f' to be executed in a
  /// synchronized way and waits for its end.
  wait ac f = (
    var done_ = false;
    send ac \(f (); done_ := true);
    done_ loop (sleep ac.millis)),

  /// `[stop ac]` stops the actor 'ac'.
  ///
  /// Notes:
  /// : Messages after stop will not be executed.
  /// : An actor stopped can not be started again.
  stop ac = send ac \(ac.cont := false)
}